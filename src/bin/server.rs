extern crate chrono;
extern crate dbus;

use dbus::tree::{Factory, MTFnMut, MethodInfo};
use dbus::{BusType, Connection, NameFlag};
use std::cell::RefCell;
use std::io::Write;
use std::net::TcpStream;
use std::rc::Rc;

const IRC_LOGIN: &'static [u8] = b"CAP LS\n
PASS icarus\n
NICK notifybot\n
USER notifybot notifybot icarus.but :notifybot\n
JOIN #notify\n
PRIVMSG #notify :starting up\n";

fn main() {
    println!("starting server");

    let stream = Rc::new(RefCell::new(TcpStream::connect("127.0.0.1:7000").unwrap()));
    let stream_c = Rc::clone(&stream);
    stream.borrow_mut().write(IRC_LOGIN).unwrap();

    let c = Connection::get_private(BusType::Session).unwrap();
    c.register_name("io.dust.notify", NameFlag::ReplaceExisting as u32)
        .unwrap();

    // create a simple factory (other options are mut and sync
    let f = Factory::new_fnmut::<()>();

    let notify_closure = move |m: &MethodInfo<MTFnMut<()>, ()>| {
        let (time, cwd, msg): (&str, &str, &str) = m.msg.read3()?;

        let line = format!("{}    // {} {}", msg, time, cwd);
        println!("{}", line);

        stream
            .borrow_mut()
            .write(format!("PRIVMSG #notify :{}\n", line).as_bytes())
            .unwrap();

        let mret = m.msg.method_return();
        Ok(vec![mret])
    };

    // create a tree with one object
    let tree = f.tree(()).add(
        f.object_path("/io/dust/notify", ()).introspectable().add(
            // add an interface to object path
            f.interface("io.dust.notify", ()).add_m(
                // and a method inside
                f.method("notify", (), notify_closure)
                    .inarg::<&str, _>("time")
                    .inarg::<&str, _>("cwd")
                    .inarg::<&str, _>("msg"),
            ),
        ),
    );

    tree.set_registered(&c, true).unwrap();

    c.add_handler(tree);

    loop {
        c.incoming(5000).next();

        stream_c
            .borrow_mut()
            .write(b"PONG localhost.localdomain\n")
            .unwrap();
    }
}
