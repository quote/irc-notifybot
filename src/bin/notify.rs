extern crate chrono;
extern crate dbus;

use chrono::Local;
use dbus::{BusType, Connection, Message};
use std::env;

fn main() {
    let now = format!("{}", Local::now().format("%Y-%m-%d %H:%M:%S"));
    let cwd = env::current_dir().unwrap();
    let cwd = cwd.to_str().unwrap();

    let mut args = env::args();
    if (args.len()) <= 1 {
        println!("usage: client message");
        return;
    }
    args.next().unwrap(); // skip process name

    let mut msg = format!("{}", args.next().unwrap());
    for argument in args {
        msg += &format!(" {}", argument);
    }

    let c = Connection::get_private(BusType::Session).unwrap();
    let m = Message::new_method_call(
        "io.dust.notify",
        "/io/dust/notify",
        "io.dust.notify",
        "notify",
    ).unwrap()
        .append3(now, cwd, msg);
    c.send_with_reply_and_block(m, 2000).unwrap();
}
