#!/bin/bash
# spins up miniircd, sleeps a few seconds, then starts up the server

pgrep -f miniircd > /dev/null || {
    # miniircd is from https://github.com/jrosdahl/miniircd
    echo "starting miniircd"
    ./miniircd --listen=127.0.0.1 --ports=7000 --password icarus --log-file=miniircd.log &
    miniircd_pid=$!

    echo "connect with"
    echo "  /rmreconnect"
    echo "  /connect icarus.irc"

    sleep 10s
}

target/release/server &
server_pid=$!

function cleanup () {
    echo "shutting down"
    # `-$$`: $$ is shell pid, - means kill process group of that pid, you have
    # to prefix with `--` so it doesn't treat it as a signal number
    trap - SIGTERM && kill -- -$$
}
trap cleanup SIGINT SIGTERM EXIT

if [ "$miniircd_pid" ]; then
    wait "$miniircd_pid" "$server_pid"
else
    wait "$server_pid"
fi
