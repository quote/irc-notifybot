#!/bin/bash
# spins up miniircd, connects to it, tails ~/notify.log, posts to #notify channel

# miniircd is from https://github.com/jrosdahl/miniircd
echo "starting miniircd and sleeping 60s"
./miniircd --listen=127.0.0.1 --ports=7000 --password icarus --log-file=miniircd.log &
# sleep for 60s to allow irssi to reconnect (or manually via):
#   /rmreconnect
#   /connect icarus.irc
sleep 60s

channel="#notify"
pipe=$(mktemp)
notify_file=~/notify.log

touch "$pipe"
echo "pipe tmpfile: $pipe"

tail -f "$pipe" | nc localhost 7000 > /dev/null &
tail_pipe_nc_pid=$!
echo "pipe -> nc started (pid: $tail_pipe_nc_pid)"

echo "
CAP LS
PASS icarus
NICK notifybot
USER notifybot notifybot icarus.bot :notifybot
JOIN $channel
PRIVMSG $channel :starting up" > "$pipe"

tail -n 0 -F "$notify_file" | awk "{ print \"PRIVMSG $channel :\"\$0; fflush(stdout) }" >> "$pipe" &
tail_notify_pipe_pid=$!
echo "tail $notify_file -> pipe started (pid: $tail_notify_pipe_pid)"

# randomly reply with pong in case the server pinged us
while true ; do echo -e "PONG localhost.localdomain" >> "$pipe" ; sleep 60s ; done &
pong_sleep_pid=$!
echo "pong started (pid: $pong_sleep_pid)"

function cleanup () {
    echo "shutting down"
    trap - SIGTERM && kill -- -$$
    rm "$pipe"
}
# `-$$`: $$ is shell pid, - means kill process group of that pid, you have to prefix with `--` so it doesn't treat it as a signal number
trap cleanup SIGINT SIGTERM EXIT

wait "$tail_pipe_nc_pid" "$tail_notify_pipe_pid" "$pong_sleep_pid"
