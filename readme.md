irc notify system.

uses miniircd from https://github.com/jrosdahl/miniircd (432513e70). download
and place python script in this folder.

build rust server/client binaries with `cargo build --release`

then use `notifybot.sh` to start up `miniircd` and the server. this connects
to the irc network and joins `#notify`. it also sets up a dbus server at
`io.dust.notify` that the `notify` rust binary connects to.

use `notify` to send messages from anywhere to the dbus server which will
post to `#notify`.

    notify hello world
