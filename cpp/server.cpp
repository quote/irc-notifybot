// clang++ -std=c++17 -Wall $(pkg-config --cflags --libs libsystemd) -o client client.cpp
#include <systemd/sd-bus.h>
#include <errno.h>
#include <iostream>
#include <string>
#include <vector>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

using namespace std;

// NOTE: irc stuff is just in progress, ended up skipping it and switching
// to the rust version.

const char* irc_login = R"(CAP LS
PASS icarus
NICK notifybot
USER notifybot notifybot icarus.bot :notifybot
JOIN #notify
PRIVMSG #notify :starting up)";

class IrcConn {
public:
    struct addrinfo hints;
    struct addrinfo *res;
    int sockfd;

    IrcConn() {
        int r;
        memset(&hints, 0, sizeof hints);
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;

        if((r = getaddrinfo("localhost", "7000", &hints, &res)) != 0) {
            fprintf(stderr, "getaddrinfo error: %s\n", strerror(-r));
        }

        sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
        connect(sockfd, res->ai_addr, res->ai_addrlen);

        send(sockfd, irc_login, strlen(irc_login), 0);
    }


};
IrcConn *conn = nullptr;

static int notify(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
    char *time, *msg;
    int r;

    r = sd_bus_message_read(m, "ss", &time, &msg);
    if(r < 0) {
        fprintf(stderr, "notify: failed to parse parameters: %s\n", strerror(-r));
    }

    fprintf(stdout, "time: %s, msg: %s\n", time, msg);

    return sd_bus_reply_method_return(m, "");
}

static const sd_bus_vtable notify_vtable[] = {
    SD_BUS_VTABLE_START(0),
    SD_BUS_METHOD("notify", "ss", "", notify, SD_BUS_VTABLE_UNPRIVILEGED),
    SD_BUS_VTABLE_END
};

int main(int argc, char *argv[]) {
    sd_bus_slot *slot = nullptr;
    sd_bus *bus = nullptr;
    int r;

    // conn = new IrcConn();

    r = sd_bus_open_user(&bus);
    if(r < 0) {
        fprintf(stderr, "failed to connect to user bus: %s\n", strerror(-r));
        goto finish;
    }

    r = sd_bus_add_object_vtable(
            bus,
            &slot,
            "/io/dust/notify",
            "io.dust.notify",
            notify_vtable,
            nullptr);
    if(r < 0) {
        fprintf(stderr, "failed to add vtable: %s\n", strerror(-r));
        goto finish;
    }

    r = sd_bus_request_name(bus, "io.dust.notify", 0);
    if(r < 0) {
        fprintf(stderr, "failed to acquire service name: %s\n", strerror(-r));
        goto finish;
    }

    fprintf(stdout, "starting to process bus events\n");

    for(;;) {
        r = sd_bus_process(bus, nullptr);
        if(r < 0) {
            fprintf(stderr, "failed to process bus: %s\n", strerror(-r));
            goto finish;
        }
        if(r > 0) {
            continue;
        }

        r = sd_bus_wait(bus, (uint64_t) -1);
        if(r < 0) {
            fprintf(stderr, "failed to wait on bus: %s\n", strerror(-r));
            goto finish;
        }
    }

finish:
    sd_bus_slot_unref(slot);
    sd_bus_unref(bus);

    return r < 0 ? 1 : 0;;
}
