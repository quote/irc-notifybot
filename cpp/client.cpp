// clang++ -std=c++17 -Wall $(pkg-config --cflags --libs libsystemd) -o client client.cpp
#include <chrono>
#include <errno.h>
#include <sstream>
#include <string>
#include <vector>
#include <ctime>
// #include <filesystem> // doesn't seem to be supported yet, need to add -lstdc++fs

#include <systemd/sd-bus.h>

using namespace std;

int main(int argc, char *argv[]) {
    if(argc == 1) {
        fprintf(stderr, "usage: notify message\ne.g. notify 'hello world'\n");
        return 0;
    }

    auto now_t = chrono::system_clock::to_time_t(chrono::system_clock::now());
    char now_c[128];
    strftime(now_c, sizeof(now_c), "%Y-%m-%d %H:%M:%S", localtime(&now_t));

    ostringstream oss;
    oss << argv[1];
    for(int z=2; z<argc; z++) {
        oss << " " << argv[z];
    }
    string msg = oss.str();

    // oss.str(string()); //clear string stream
    // oss << filesystem::current_path();
    // string path = oss.str();

    sd_bus_error error = SD_BUS_ERROR_NULL;
    sd_bus_message *m = nullptr;
    sd_bus *bus = nullptr;
    int r;

    r = sd_bus_open_user(&bus);
    if(r < 0) {
        fprintf(stderr, "failed to connect to user bus: %s\n", strerror(-r));
    }

    r = sd_bus_call_method(
            bus,
            "io.dust.notify",
            "/io/dust/notify",
            "io.dust.notify",
            "notify",
            &error,
            &m,
            "ss",
            now_c,
            // path.c_str(),
            msg.c_str());
    if(r < 0) {
        fprintf(stderr, "failed to issue method call: error.msg: %s, strerror(-r): %s\n", error.message, strerror(-r));
        goto finish;
    }

finish:
    sd_bus_error_free(&error);
    sd_bus_message_unref(m);
    sd_bus_unref(bus);

    return r < 0 ? 1 : 0;;
}
